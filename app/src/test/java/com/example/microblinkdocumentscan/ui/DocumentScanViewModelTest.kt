package com.example.microblinkdocumentscan.ui

import app.cash.turbine.test
import com.microblink.blinkid.data.Document
import com.microblink.blinkid.repositories.DocumentRepository
import com.microblink.blinkid.ui.ScanDocumentViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import com.google.common.truth.Truth.assertThat
import com.microblink.blinkid.ui.ScanDocumentFragmentDirections
import kotlinx.coroutines.flow.flow
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class DocumentScanViewModelTest {

    private val documentRepositoryMock = mockk<DocumentRepository>()
    private lateinit var scanDocumentViewModel: ScanDocumentViewModel


    val document = mockk<Document>()
    val documentList = listOf(document)
    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        scanDocumentViewModel = ScanDocumentViewModel(documentRepositoryMock)
    }

    @Test
    fun `get list of saved documents`() {
        coEvery { documentRepositoryMock.observeDocuments() } returns flowOf(documentList)
        runBlockingTest {
            scanDocumentViewModel.savedDocuments.test {
                assertThat(awaitItem()).isNotEmpty()
                cancelAndConsumeRemainingEvents()
            }
        }
    }

    @Test
    fun `select document and navigate to details page`() {
        runBlockingTest {
            scanDocumentViewModel.documentSelected(document)
            scanDocumentViewModel.navigation.test {
                scanDocumentViewModel.documentSelected(document)
                assertThat(awaitItem()).isEqualTo(
                    ScanDocumentFragmentDirections.documentScanned(
                        document
                    )
                )
            }
        }

    }

    @Test
    fun `scan success navigate to details page`() {
        runBlockingTest {
            scanDocumentViewModel.navigation.test {
                scanDocumentViewModel.documentScanned(document)
                assertThat(awaitItem()).isEqualTo(
                    ScanDocumentFragmentDirections.documentScanned(
                        document
                    )
                )
            }
        }
    }
}