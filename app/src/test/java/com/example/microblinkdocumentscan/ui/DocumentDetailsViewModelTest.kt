package com.example.microblinkdocumentscan.ui

import app.cash.turbine.test
import com.google.common.truth.Truth.assertThat
import com.microblink.blinkid.R
import com.microblink.blinkid.data.Document
import com.microblink.blinkid.repositories.DocumentRepository
import com.microblink.blinkid.ui.DocumentDetailsViewModel
import com.microblink.blinkid.ui.ScanDocumentFragmentDirections
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class DocumentDetailsViewModelTest {

    private val documentRepositoryMock = mockk<DocumentRepository>()
    private val documentDetailsViewModel = DocumentDetailsViewModel(documentRepositoryMock)

    private val document = Document(
        holderName = "John",
        holderSurname = "Doe",
        documentHolderImage = mockk(),
        documentNumber = "00000",
        frontDocumentImage = mockk(),
        backDocumentImage = mockk(),
        sex = "male",
        nationality = "bih",
        expiryDate = LocalDate.of(1,1,1),
        birthDate = LocalDate.now(),
        personalIdentificationNumber = "12121"
    )
    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup(){
        Dispatchers.setMain(dispatcher)
        documentDetailsViewModel.document.value = document

    }

    @Test
    fun `document exists in database set indicator true`(){
        coEvery { documentRepositoryMock.documentExists("1212") } answers { true }
        documentDetailsViewModel.isDocumentSaved("1212")
        runBlockingTest {
            documentDetailsViewModel.documentSaved.test {
                assertThat(awaitItem()).isEqualTo(true)
            }
        }
    }

    @Test
    fun `document does not exist in database set indicator false`(){
        coEvery { documentRepositoryMock.documentExists("1212") } answers { false }
        documentDetailsViewModel.isDocumentSaved("1212")
        runBlockingTest {
            documentDetailsViewModel.documentSaved.test {
                assertThat(awaitItem()).isEqualTo(false)
            }
        }
    }

    @Test
    fun `insert new document successful show message`(){
        coEvery { documentRepositoryMock.insertDocument(document) } answers {}
        coEvery { documentRepositoryMock.documentCount() } returns (1)
        runBlockingTest {
            documentDetailsViewModel.toastMessage.test {
                documentDetailsViewModel.saveDocument()
                assertThat(awaitItem()).isNotNull()
                cancelAndConsumeRemainingEvents()
            }
        }
    }

    @Test
    fun `delete document from database show message`(){
        coEvery { documentRepositoryMock.deleteDocument(document) } answers {}
        runBlockingTest {
            documentDetailsViewModel.toastMessage.test {
                documentDetailsViewModel.deleteDocument()
                assertThat(awaitItem()).isNotNull()
                cancelAndConsumeRemainingEvents()
            }
        }
    }

    @Test
    fun `document expired show error message`(){
        val date = LocalDate.parse("20.04.1997." , DateTimeFormatter.ofPattern("dd.MM.yyyy."))
        documentDetailsViewModel.validateDocument(date)
        runBlockingTest {
            documentDetailsViewModel.isValid.test{
                assertThat(awaitItem()).isFalse()
            }
        }
    }

    @Test
    fun `document valid update indicator`(){
        val date = LocalDate.parse("20.05.2024.", DateTimeFormatter.ofPattern("dd.MM.yyyy."))
        documentDetailsViewModel.validateDocument(date)
        runBlockingTest {
            documentDetailsViewModel.isValid.test {
                assertThat(awaitItem()).isTrue()
            }
        }
    }

    @Test
    fun `try to save document max number of documents in database show message`(){
        coEvery { documentRepositoryMock.documentCount() } returns(5)
        runBlockingTest {
            documentDetailsViewModel.toastMessage.test {
                documentDetailsViewModel.saveDocument()
                assertThat(awaitItem()).isEqualTo(R.string.you_reached_max_number_of_saved_documents)
            }
        }
    }
}