package com.microblink.blinkid.utils

object Constants {

    const val DATABASE_NAME = "document-db"
    const val DOCUMENT_MAX_ROW_COUNT = 5
    const val BLINKID_LICENCE_KEY_PATH = "com.microblink.blinkid.mblic"
}