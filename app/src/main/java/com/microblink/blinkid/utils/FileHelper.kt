package com.microblink.blinkid.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileHelper @Inject constructor(@ApplicationContext private val context: Context) {

    fun saveToInternalStorage(bitmap: Bitmap, imageName: String): String {
        val directory = context.getDir("imageDir", Context.MODE_PRIVATE)
        val filePath = File(directory, imageName)
        lateinit var fileOutputStream: FileOutputStream

        try {
            fileOutputStream = FileOutputStream(filePath)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fileOutputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return filePath.absolutePath
    }

    fun loadFromInternalStorage(documentPath: String?): Bitmap? {
        if( documentPath != null) {
            val file = File(documentPath)
            return BitmapFactory.decodeStream(FileInputStream(file))
        }
        return null

    }

    fun deleteFromInternalStorage(documentPath: String?) {
        if (documentPath != null) {
            val file = File(documentPath)
            file.delete()
        }
    }

}