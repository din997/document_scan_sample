package com.microblink.blinkid.utils

import android.graphics.Bitmap
import android.widget.ImageView
import com.microblink.blinkid.GlideApp
import com.microblink.blinkid.R


fun ImageView.load(bitmap: Bitmap?){
    GlideApp.with(this).load(bitmap).placeholder(R.mipmap.ic_launcher).into(this)
}