package com.microblink.blinkid

import android.app.Application
import com.microblink.MicroblinkSDK
import com.microblink.blinkid.utils.Constants
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DocumentScanApplication: Application(){

    override fun onCreate() {
        super.onCreate()
        MicroblinkSDK.setLicenseFile(Constants.BLINKID_LICENCE_KEY_PATH, this)
    }
}