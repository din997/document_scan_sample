package com.microblink.blinkid.repositories

import com.microblink.blinkid.data.Document
import com.microblink.blinkid.data.DocumentEntity
import com.microblink.blinkid.data.DocumentScanDao
import com.microblink.blinkid.utils.FileHelper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DocumentRepository @Inject constructor(
    private val documentScanDao: DocumentScanDao,
    private val fileHelper: FileHelper
) {

    suspend fun insertDocument(document: Document) {
        val frontImage = if (document.frontDocumentImage != null) fileHelper.saveToInternalStorage(
            document.frontDocumentImage,
            "${document.documentNumber}Front"
        ) else null
        val backImage =
            if (document.backDocumentImage != null) fileHelper.saveToInternalStorage(
                document.backDocumentImage,
                "${document.documentNumber}Back"
            ) else null

        val profileImage =
            if (document.documentHolderImage != null) fileHelper.saveToInternalStorage(
                document.documentHolderImage,
                "${document.documentNumber}Face"
            ) else null
        val documentEntity = DocumentEntity(
            documentNumber = document.documentNumber,
            holderName = document.holderName,
            holderSurname = document.holderSurname,
            sex = document.sex,
            nationality = document.nationality,
            expiryDate = document.expiryDate,
            birthDate = document.birthDate,
            personalIdentificationNumber = document.personalIdentificationNumber,
            documentHolderImage = profileImage,
            frontDocumentImage = frontImage,
            backDocumentImage = backImage
        )
        documentScanDao.insertDocument(documentEntity)
    }

    fun observeDocuments(): Flow<List<Document>> {
        return documentScanDao.observeAllDocuments().map {
            it.map { documentEntity ->
                val frontImage = fileHelper.loadFromInternalStorage(documentEntity.frontDocumentImage)
                val backImage = fileHelper.loadFromInternalStorage(documentEntity.backDocumentImage)
                val holderImage = fileHelper.loadFromInternalStorage(documentEntity.documentHolderImage)
                val document = Document(
                    documentNumber = documentEntity.documentNumber,
                    holderName = documentEntity.holderName,
                    holderSurname = documentEntity.holderSurname,
                    sex = documentEntity.sex,
                    nationality = documentEntity.nationality,
                    expiryDate = documentEntity.expiryDate,
                    birthDate = documentEntity.birthDate,
                    personalIdentificationNumber = documentEntity.personalIdentificationNumber,
                    documentHolderImage = holderImage,
                    backDocumentImage = backImage,
                    frontDocumentImage = frontImage
                )
                document
            }
        }
    }

    suspend fun deleteDocument(document: Document) {
        val documentEntity = documentScanDao.getDocumentByNumber(document.documentNumber)
        documentScanDao.deleteDocument(documentEntity)
        fileHelper.deleteFromInternalStorage(documentEntity.frontDocumentImage)
        fileHelper.deleteFromInternalStorage(documentEntity.backDocumentImage)
        fileHelper.deleteFromInternalStorage(documentEntity.documentHolderImage)
    }

    suspend fun documentExists(documentNumber: String): Boolean {
        return documentScanDao.documentExists(documentNumber)
    }

    suspend fun documentCount(): Int{
        return documentScanDao.getDocumentCount()
    }


}