package com.microblink.blinkid.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {

    val navigation: MutableSharedFlow<NavDirections> = MutableSharedFlow()
    val backPressedEvent: MutableSharedFlow<Unit> = MutableSharedFlow()
    val toastMessage: MutableSharedFlow<Int> = MutableSharedFlow()

    protected fun navigateTo(destination: NavDirections) = viewModelScope.launch {
        navigation.emit(destination)
    }

    protected fun goBack() = viewModelScope.launch {
        backPressedEvent.emit(Unit)
    }

    protected fun showToastMessage(message: Int) = viewModelScope.launch {
        toastMessage.emit(message)
    }
}