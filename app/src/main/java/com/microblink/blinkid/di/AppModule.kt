package com.microblink.blinkid.di

import android.content.Context
import androidx.room.Room
import com.microblink.blinkid.data.DocumentScanDatabase
import com.microblink.blinkid.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDocumentScanDatabase(@ApplicationContext context: Context)=
         Room.databaseBuilder(context, DocumentScanDatabase::class.java, Constants.DATABASE_NAME)
            .build()

    @Singleton
    @Provides
    fun provideDocumentDao(db: DocumentScanDatabase) =
        db.documentScanDao()
}