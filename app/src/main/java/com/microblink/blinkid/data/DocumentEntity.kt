package com.microblink.blinkid.data

import android.graphics.Bitmap
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.microblink.blinkid.utils.FileHelper
import kotlinx.android.parcel.Parcelize
import java.time.LocalDate



@Entity(tableName = "documents")
data class DocumentEntity(
    @PrimaryKey
    val documentNumber: String,
    val holderName: String,
    val holderSurname: String,
    val sex: String,
    val nationality:String,
    val expiryDate:LocalDate,
    val birthDate: LocalDate,
    val personalIdentificationNumber: String,
    val documentHolderImage: String?,
    val frontDocumentImage: String?,
    val backDocumentImage: String?
)

@Parcelize
data class Document(
    val holderName: String,
    val holderSurname: String,
    val sex: String,
    val nationality:String,
    val documentNumber: String,
    val expiryDate:LocalDate,
    val birthDate: LocalDate,
    val personalIdentificationNumber: String,
    val documentHolderImage: Bitmap?,
    val frontDocumentImage: Bitmap?,
    val backDocumentImage: Bitmap?
):Parcelable