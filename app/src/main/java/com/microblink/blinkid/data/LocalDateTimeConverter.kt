package com.microblink.blinkid.data

import androidx.room.TypeConverter
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId

class LocalDateTimeConverter {

    @TypeConverter
    fun dateToTimeStamp(date: LocalDate?): Long? {
        if(date == null){
            return null
        }

        return date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()

    }

    @TypeConverter
    fun timeStampToDate(timestamp:Long?): LocalDate? {
        if(timestamp == null){
            return null
        }
        return Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDate()
    }
}