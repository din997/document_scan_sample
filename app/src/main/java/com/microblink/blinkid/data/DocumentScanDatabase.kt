package com.microblink.blinkid.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(
    entities = [DocumentEntity::class],
    version = 1
)
@TypeConverters(
    LocalDateTimeConverter::class
)
abstract class DocumentScanDatabase: RoomDatabase() {

    abstract  fun documentScanDao(): DocumentScanDao
}