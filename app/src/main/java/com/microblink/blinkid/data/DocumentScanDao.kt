package com.microblink.blinkid.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface DocumentScanDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDocument(document: DocumentEntity)

    @Query("SELECT * FROM documents WHERE documentNumber = :documentNumber")
    suspend fun getDocumentByNumber(documentNumber: String): DocumentEntity

    @Delete
    suspend fun deleteDocument(document: DocumentEntity)

    @Query("SELECT * FROM documents")
    fun observeAllDocuments(): Flow<List<DocumentEntity>>

    @Query("SELECT EXISTS (SELECT 1 FROM documents WHERE documentNumber = :documentNumber)")
    suspend fun documentExists(documentNumber: String): Boolean

    @Query("SELECT COUNT(documentNumber) FROM documents")
    suspend fun getDocumentCount(): Int
}