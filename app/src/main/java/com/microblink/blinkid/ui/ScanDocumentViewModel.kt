package com.microblink.blinkid.ui

import com.microblink.blinkid.base.BaseViewModel
import com.microblink.blinkid.data.Document
import com.microblink.blinkid.data.DocumentEntity
import com.microblink.blinkid.repositories.DocumentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject


@HiltViewModel
class ScanDocumentViewModel @Inject constructor(private val documentRepository: DocumentRepository): BaseViewModel() {

    val savedDocuments: Flow<List<Document>> by lazy {
        documentRepository.observeDocuments()
    }

    val documentListEmpty = MutableStateFlow(false)

    fun documentScanned(document: Document){
       navigateTo(ScanDocumentFragmentDirections.documentScanned(document))
    }

    fun documentSelected(document: Document){
        navigateTo(ScanDocumentFragmentDirections.documentScanned(document))
    }

}