package com.microblink.blinkid.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microblink.blinkid.data.Document
import com.microblink.blinkid.databinding.DocumentItemLayoutBinding
import com.microblink.blinkid.utils.load

class DocumentsAdapter(private val onItemClickListener: (document: Document) -> Unit) : RecyclerView.Adapter<DocumentViewHolder>() {

    var documentList = listOf<Document>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentViewHolder {
        return DocumentViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: DocumentViewHolder, position: Int) {
        val document = documentList[position]
        holder.bind(document)
        holder.itemView.setOnClickListener {
            onItemClickListener.invoke(document)
        }
    }

    override fun getItemCount() = documentList.size

}

class DocumentViewHolder(private val binding: DocumentItemLayoutBinding) :
    RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun create(parent: ViewGroup): DocumentViewHolder {
            return DocumentViewHolder(
                DocumentItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    fun bind(document: Document) {
        binding.holderName.text = document.holderName
        binding.holderSurname.text = document.holderSurname
        binding.personImage.load(document.documentHolderImage)
    }
}