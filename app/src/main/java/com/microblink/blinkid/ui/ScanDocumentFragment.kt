package com.microblink.blinkid.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.microblink.blinkid.R
import com.microblink.blinkid.base.BaseFragment
import com.microblink.blinkid.data.DocumentEntity
import com.microblink.MicroblinkSDK
import com.microblink.blinkid.data.Document
import com.microblink.blinkid.databinding.FragmentScanDocumentBinding
import com.microblink.entities.recognizers.RecognizerBundle
import com.microblink.entities.recognizers.blinkid.generic.BlinkIdCombinedRecognizer
import com.microblink.intent.IntentDataTransferMode
import kotlinx.android.synthetic.main.fragment_scan_document.*
import com.microblink.uisettings.ActivityRunner.startActivityForResult
import com.microblink.uisettings.BlinkIdUISettings
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@AndroidEntryPoint
class ScanDocumentFragment : BaseFragment<FragmentScanDocumentBinding, ScanDocumentViewModel>() {
    override val layoutId = R.layout.fragment_scan_document
    override val viewModel by viewModels<ScanDocumentViewModel>()

    private val recognizer = BlinkIdCombinedRecognizer()
    var recognizerBundle: RecognizerBundle? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = DocumentsAdapter {
            viewModel.documentSelected(it)
        }
        btn_id_card_scan.setOnClickListener {
            scanDocument()
        }

        rv_documents.layoutManager = LinearLayoutManager(context)
        rv_documents.adapter = adapter
        rv_documents.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.savedDocuments.collectLatest {
                adapter.documentList = it
                viewModel.documentListEmpty.value = it.isEmpty()
            }
        }
    }

    private fun scanDocument() {
        MicroblinkSDK.setIntentDataTransferMode(IntentDataTransferMode.OPTIMISED)
        recognizer.setEncodeFullDocumentImage(true)
        recognizer.setEncodeFaceImage(true)
        recognizerBundle = RecognizerBundle(recognizer)
        val settings = BlinkIdUISettings(recognizerBundle!!)
        startActivityForResult(this, 0, settings)

    }

    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                recognizerBundle?.loadFromIntent(data)

                val documentRaw = recognizer.result
                val birthDate = LocalDate.parse(
                    documentRaw.dateOfBirth.date.toString(),
                    DateTimeFormatter.ofPattern("dd.MM.yyyy.")
                )
                val dateOfExpiry = LocalDate.parse(
                    documentRaw.dateOfExpiry.date.toString(),
                    DateTimeFormatter.ofPattern("dd.MM.yyyy.")
                )

                val frontImage = BitmapFactory.decodeByteArray(
                    documentRaw.encodedFrontFullDocumentImage,
                    0,
                    documentRaw.encodedFrontFullDocumentImage.size
                )
                val backImage = BitmapFactory.decodeByteArray(
                    documentRaw.encodedBackFullDocumentImage,
                    0,
                    documentRaw.encodedBackFullDocumentImage.size
                )
                val documentHolderImage = BitmapFactory.decodeByteArray(
                    documentRaw.encodedFaceImage,
                    0,
                    documentRaw.encodedFaceImage.size
                )

                val document = Document(
                    holderName = documentRaw.firstName,
                    holderSurname = documentRaw.lastName,
                    sex = documentRaw.sex,
                    expiryDate = dateOfExpiry,
                    birthDate = birthDate,
                    nationality = documentRaw.nationality,
                    documentNumber = documentRaw.documentNumber,
                    personalIdentificationNumber = documentRaw.personalIdNumber,
                    backDocumentImage = backImage,
                    frontDocumentImage = frontImage,
                    documentHolderImage = documentHolderImage
                )

                viewModel.documentScanned(document)
            }
        }

    }
}

