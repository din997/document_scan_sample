package com.microblink.blinkid.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.microblink.blinkid.R
import com.microblink.blinkid.base.BaseViewModel
import com.microblink.blinkid.data.Document
import com.microblink.blinkid.repositories.DocumentRepository
import com.microblink.blinkid.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.ZoneId
import javax.inject.Inject

@HiltViewModel
class DocumentDetailsViewModel @Inject constructor(private val documentRepository: DocumentRepository) :
    BaseViewModel() {

    val document = MutableStateFlow<Document?>(null)
    val isValid = MutableStateFlow(true)
    val documentSaved = MutableStateFlow(false)


    fun validateDocument(dateOfExpiry: LocalDate) {
        viewModelScope.launch {
            val compare = dateOfExpiry.compareTo(LocalDate.now())
            isValid.value = compare > 0
        }
    }

    fun saveDocument() {
        viewModelScope.launch {
            val documentCount = documentRepository.documentCount()
            if(documentCount >= Constants.DOCUMENT_MAX_ROW_COUNT){
                showToastMessage(R.string.you_reached_max_number_of_saved_documents)
            }
            else {
                documentRepository.insertDocument(document.value!!)
                showToastMessage(R.string.saved_document_to_database)
                goBack()
            }
        }
    }

    fun isDocumentSaved(documentNumber: String) {
        viewModelScope.launch {
            documentSaved.value = documentRepository.documentExists(documentNumber)
        }
    }

    fun deleteDocument(){
        viewModelScope.launch{
            documentRepository.deleteDocument(document.value!!)
            showToastMessage(R.string.document_deleted_from_db)
            goBack()

        }
    }


}