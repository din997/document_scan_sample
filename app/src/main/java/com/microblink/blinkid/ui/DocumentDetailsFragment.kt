package com.microblink.blinkid.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.microblink.blinkid.GlideApp
import com.microblink.blinkid.R
import com.microblink.blinkid.base.BaseFragment
import com.microblink.blinkid.databinding.FragmentDocumentDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.time.format.DateTimeFormatter


@AndroidEntryPoint
class DocumentDetailsFragment :
    BaseFragment<FragmentDocumentDetailsBinding, DocumentDetailsViewModel>() {
    override val layoutId = R.layout.fragment_document_details
    override val viewModel by viewModels<DocumentDetailsViewModel>()
    private val args by navArgs<DocumentDetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.document.value = args.document

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.document.collect {
                binding!!.birthDate.text = getString(
                    R.string.birth_date,
                    it?.birthDate?.format(DateTimeFormatter.ofPattern("dd.MM.yyyy."))
                )

                binding!!.expiryDate.text = getString(
                    R.string.expiry_date,
                    it?.expiryDate?.format(DateTimeFormatter.ofPattern("dd.MM.yyyy."))
                )

                GlideApp.with(this@DocumentDetailsFragment).load(it?.frontDocumentImage)
                    .into(binding!!.frontImage)

                GlideApp.with(
                    this@DocumentDetailsFragment
                ).load(it?.backDocumentImage)
                    .into(binding!!.backImage)

                GlideApp.with(
                    this@DocumentDetailsFragment
                ).load(it?.documentHolderImage)
                    .into(binding!!.personImage)

                viewModel.validateDocument(it!!.expiryDate)
                viewModel.isDocumentSaved(it.documentNumber)

            }
        }
    }
}